# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'VidzioIframePlugin.description'
        db.delete_column(u'cmsplugin_vidzioiframeplugin', 'description')

        # Deleting field 'VidzioIframePlugin.cover'
        db.delete_column(u'cmsplugin_vidzioiframeplugin', 'cover_id')

        # Deleting field 'VidzioIframePlugin.title'
        db.delete_column(u'cmsplugin_vidzioiframeplugin', 'title')

        # Deleting field 'VidzioIframePlugin.long_description'
        db.delete_column(u'cmsplugin_vidzioiframeplugin', 'long_description')


    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'VidzioIframePlugin.description'
        raise RuntimeError("Cannot reverse this migration. 'VidzioIframePlugin.description' and its values cannot be restored.")
        # Adding field 'VidzioIframePlugin.cover'
        db.add_column(u'cmsplugin_vidzioiframeplugin', 'cover',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['filer.Image'], null=True, blank=True),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'VidzioIframePlugin.title'
        raise RuntimeError("Cannot reverse this migration. 'VidzioIframePlugin.title' and its values cannot be restored.")

        # User chose to not deal with backwards NULL issues for 'VidzioIframePlugin.long_description'
        raise RuntimeError("Cannot reverse this migration. 'VidzioIframePlugin.long_description' and its values cannot be restored.")

    models = {
        'cms.cmsplugin': {
            'Meta': {'object_name': 'CMSPlugin'},
            'changed_date': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'creation_date': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language': ('django.db.models.fields.CharField', [], {'max_length': '15', 'db_index': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cms.CMSPlugin']", 'null': 'True', 'blank': 'True'}),
            'placeholder': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['cms.Placeholder']", 'null': 'True'}),
            'plugin_type': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_index': 'True'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        'cms.placeholder': {
            'Meta': {'object_name': 'Placeholder'},
            'default_width': ('django.db.models.fields.PositiveSmallIntegerField', [], {'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'slot': ('django.db.models.fields.CharField', [], {'max_length': '50', 'db_index': 'True'})
        },
        u'cmsplugin_vidzio_iframe.vidzioiframeplugin': {
            'Meta': {'object_name': 'VidzioIframePlugin', 'db_table': "u'cmsplugin_vidzioiframeplugin'", '_ormbases': ['cms.CMSPlugin']},
            u'cmsplugin_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['cms.CMSPlugin']", 'unique': 'True', 'primary_key': 'True'}),
            'height': ('django.db.models.fields.IntegerField', [], {'default': '480'}),
            'timestamp': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'vidzio_id': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'blank': 'True'}),
            'width': ('django.db.models.fields.IntegerField', [], {'default': '640'})
        }
    }

    complete_apps = ['cmsplugin_vidzio_iframe']