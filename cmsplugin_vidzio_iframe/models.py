from django.db import models
from cms.models import CMSPlugin

class VidzioIframePlugin(CMSPlugin):
    vidzio_id = models.SlugField(blank=True)
    timestamp = models.CharField(max_length=20, blank=True)
    width = models.IntegerField(default=640)
    height = models.IntegerField(default=480)

    class Meta:
        db_table = 'cmsplugin_vidzio_iframe_vidzioiframeplugin'

    def __unicode__(self):
        return u'Vidzio %s' % (self.vidzio_id)

