from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from django.utils.translation import ugettext as _

from .models import VidzioIframePlugin as VidzioModel

class VidzioIframePlugin(CMSPluginBase):
    model = VidzioModel
    module = 'Vidzor'
    name = _('Vidzio iframe')
    render_template = 'plugins/vidzio_iframe.html'
    text_enabled = True

    def render(self, context, instance, placeholder):
        context.update({'instance': instance})
        return context

plugin_pool.register_plugin(VidzioIframePlugin)
