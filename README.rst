cmsplugin_vidzio_iframe
========================

This plugin allows easy embedding of [Vidzor videos](http://vidzor.com) under django-cms.

Installation
-------------

After installing this repo, you should add `cmsplugin_vidzio_iframe` to `INSTALLED_APPS`, and run `python manage.py syncdb --migrate`.

Usage
------

Select the plugin from the plugin dropdown, and add it to your CMS editor area. The plugin can be used inside the text plugin too.